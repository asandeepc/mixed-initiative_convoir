# coding: utf-8
import sys
import datetime
import json
import time
from bs4 import BeautifulSoup
from slackclient import SlackClient


BOT_ID= "" ## Setup the bot id here. 
EXP_CHANNEL= "" ## Set the Slack channel id here. 
SLACK_BOT_TOKEN=''## Set the token id. You get this when you first create the app. 
AT_BOT = "<@" + BOT_ID + ">" # don't use the :; HATE it!
EXAMPLE_COMMAND = "do"
slack_client = SlackClient(SLACK_BOT_TOKEN)

OUT_DIR = ''

flag_user_request = True

flag_wizard_response = False

def write_to_file(output):
    global output_file
    #json.dump(output,output_file,indent=4, separators=(',', ': '))
    a_dict = {'timestamp': datetime.datetime.utcnow().isoformat()}
    a_dict2 = {'task': task}
    a_dict3 = {'level': condition}
    a_dict4 = {'sessionid': sessionid}
    output.update(a_dict)
    output.update(a_dict2)
    output.update(a_dict3)
    output.update(a_dict4)
    json.dump(output, output_file)
    output_file.write("\n")
    output_file.flush()

task = int(sys.argv[1])
condition = int(sys.argv[2])
fileinput = sys.argv[3]
timestp = datetime.datetime.utcnow().isoformat().replace(":","_")
sessionid = fileinput # This is also the session id
output_file = open(OUT_DIR+fileinput+"_"+str(task)+"_"+timestp+"_.json", "w")



def parse_slack_output(slack_rtm_output):
    """
        The Slack Real Time Messaging API is an events firehose.
        this parsing function returns None unless a message is
        directed at the Bot, based on its ID.
    """
    output_list = slack_rtm_output

    global flag_user_request
    global flag_wizard_response
    print("Output_list:"+str(output_list))
    if output_list and len(output_list) > 0:
        for output in output_list:
            print("Out list:")
            try:
                print("raw output:"+str(output))
            except:
                print("This was an exception case")
            write_to_file(output)

            if 'user' in output:
                if "U7SE6HFKSBKHVNZJ77E" == output['user']:## Replace the id here with your own. # The point here is that the user's flag, which helps adding the emoticon will only be refreshed after the Wizard has responded.
                    print("********** I AM HERE ***************")
                    if flag_wizard_response and (not flag_user_request) and ('text' in output):
                        flag_user_request = True
                        flag_wizard_response = False


            if output and 'text' in output and AT_BOT in output['text']:
            #if output and 'text' in output and AT_WIZARD in output['text']:
                # return text after the @ mention, whitespace removed
                print("printing channel")
                print("after parsing:"+str(output['channel']))


                if AT_BOT in output['text']: # if AT_BOT in output['text']: Earlier approach

                    if flag_user_request:
                    ## When ever this message is flagged up, turn the flag_user_request to false until the Wizard responds back.
                        slack_client.api_call("reactions.add", channel=EXP_CHANNEL, name="heavy_check_mark", timestamp=output["event_ts"])
                        flag_user_request = False
                        flag_wizard_response = True

                return output['text'].split(AT_BOT)[1].strip().lower(), \
                       EXP_CHANNEL

    return None, None

if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1 # 1 second delay between reading from firehose
    #print(WIZARD_ID)
    if slack_client.rtm_connect():
        print("StarterBot connected and running!")
        while True:
            command, channel = parse_slack_output(slack_client.rtm_read())
            if(command and channel):
                print(command + " " + channel)
            #if command and channel:
            #    handle_command(command, channel)
            time.sleep(READ_WEBSOCKET_DELAY)
    else:
        print("Connection failed. Invalid Slack token or bot ID?")
