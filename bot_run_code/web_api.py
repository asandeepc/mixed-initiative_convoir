import requests
import json


subscription_key ='' # Set subscription key for the bing service.  
search_term = "burrito"

search_url = "https://api.cognitive.microsoft.com/bing/v5.0/search"

headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
params  = {"q": search_term, "textDecorations":True, "textFormat":"HTML","count":50}
response = requests.get(search_url, headers=headers, params=params)
response.raise_for_status()
search_results = response.json()
print("Hello world!")
print(search_results)

results = search_results['webPages']['value']


for res in results:
	print(res)
	print("\n")
	print("----------")
	print("\n")