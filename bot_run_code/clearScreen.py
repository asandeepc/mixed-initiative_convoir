import os
import time
from slackclient import SlackClient

BOT_ID= "" ## Setup the bot id here. 
EXP_CHANNEL= "" ## Set the Slack channel id here. 
SLACK_BOT_TOKEN=''## Set the token id. You get this when you first create the app. 


# instantiate Slack & Twilio clients
slack_client = SlackClient(SLACK_BOT_TOKEN)

# clear the chat screen
def clear_chatScreen(channel):
    response = "\n"
    slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True, unfurl_links=True,
                          color='#7CD197')
    for x in range(0,100):
        slack_client.api_call("chat.postMessage", channel=channel, text=response, as_user=True, unfurl_links=True)


if __name__ == "__main__":
    READ_WEBSOCKET_DELAY = 1 # 1 second delay between reading from firehose
    clear_chatScreen(EXP_CHANNEL)
