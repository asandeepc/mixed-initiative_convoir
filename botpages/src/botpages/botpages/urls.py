"""botpages URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from landingpages.views import landing_view, wizard_view,slash_response,wizard_login,test,logout,interaction_data, user_login,wiztest

urlpatterns = [
    path('landing/',landing_view,name = 'landing_view'),
    path('wizard/',wizard_view,name = 'wizard_view'),
    path('slash/',slash_response,name = 'slash_response'),
    path('login/',wizard_login,name = "wizard_login"),
    path('user_login/',user_login,name = "user_login"),
    path('logout/',logout,name = "logout"),
    path('interaction_data/',interaction_data,name="interaction_data"),
    path('wiztest/',wiztest,name="wiztest"),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
