from django.apps import AppConfig


class WizardpagesConfig(AppConfig):
    name = 'wizardpages'
