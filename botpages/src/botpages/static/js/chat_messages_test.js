$(document).ready(function() {
    // Below are all the messages that the Wizard types in the channel and which go to the Slack channel.
    $("#clarification2").submit(function(e){ // Condition 2 clarification.
      e.preventDefault();
      var ques_val = $("input[name='ques']").val();
      $.get("/wiztest/", { quant:ques_val, ques:ques_val, questionsbtn:"", condition:"2", type:"chat_message", interaction:true },function(response){
        $('#clarify_id').val('Ask your questions here ...');
      });
    });

    $("#clarification3").submit(function(e){ // Condition 3 clarification
      e.preventDefault();
      var ques_val = $("input[name='ques3']").val();
      // Now gonna make the ajax call to the server.
      $.get("/wiztest/", { quant:ques_val, ques:ques_val, questionsbtn3:"",condition:"3", type:"chat_message", interaction:true }, function(response){
        $('#suggestion_clarify_id').val('Ask your questions here ...');
      });
    });

    $("#suggestion3").submit(function(e){ // Condition 3 suggestion.
      e.preventDefault();
      console.log('inside suggestions');
      var sugg_val = $("input[name='sugg']").val();
      // Now gonna make the ajax call to the server.
      $.get("/wiztest/", { quant:sugg_val, sugg:sugg_val, suggestbtn:"",condition:"3", type:"chat_message", interaction:true }, function(response){
        $('#suggestion_id').val('Give suggestions here ...');
      });
    });

});
