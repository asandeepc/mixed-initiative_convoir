from django.db import models
from django.utils import timezone
from jsonfield import JSONField
# Create your models here.

class landingPages(models.Model):
	## Here I am basically just giving datatypes to the variables that will be used in the page.
	title = models.CharField(max_length=120)
	timestamp = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.title

class wizardUsers(models.Model):
	wizard_id = models.CharField(max_length=50)
	session_id = models.CharField(max_length=50)
	timestamp = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.wizard_id

class collabUsers(models.Model):
	user_id = models.CharField(max_length=200)
	session_id = models.CharField(max_length=50)
	wizard_table_key = models.ForeignKey('wizardUsers', on_delete=models.CASCADE)

	def __str__(self):
		return self.user_id

class wizardInterface(models.Model):
	query = models.CharField(max_length=400)
	searchbot_condition = models.IntegerField()
	results = JSONField()
	wizard_id = models.CharField(max_length=50)
	session_id = models.CharField(max_length=50)
	timestamp = models.DateTimeField(default=timezone.now)
	wizard_table_key = models.ForeignKey('wizardUsers', on_delete=models.CASCADE)

	def __str__(self):
		return self.query

class interactionEvents(models.Model):
	event = models.CharField(max_length=50) # Here I am logging the type of the event.
											# These could be clicks, scrolls, pagination, click rank, and mouse overs
	wizard_id = models.CharField(max_length=50)
	value = models.CharField(max_length=2000) ## This can be clicks counts and the chat messages.
	url = models.CharField(max_length=1000)
	searchbot_condition = models.IntegerField()
	#pagination_page = models.IntegerField()
	session_id = models.CharField(max_length=50)
	interface_id = models.ForeignKey('wizardInterface', on_delete=models.CASCADE)
	timestamp = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.event
