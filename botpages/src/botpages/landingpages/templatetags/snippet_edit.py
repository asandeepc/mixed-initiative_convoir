from django import template
from bs4 import BeautifulSoup


register = template.Library()

@register.filter

def snippet_edit(value):
	temp_str = BeautifulSoup(value, 'html.parser').get_text().replace("?", "'")
	return temp_str