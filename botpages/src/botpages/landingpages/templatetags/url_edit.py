from django import template
from bs4 import BeautifulSoup


register = template.Library()

@register.filter

# def returnHTTPSurl(url):
#     if url.startswith("http"):
#         return url
#     else:
#         return "https://"+url


def url_edit(value):
	url_edited = BeautifulSoup(value, 'html.parser').get_text()
	if url_edited.startswith("http"):
		return url_edited
	else:
		return "https://"+url_edited		
