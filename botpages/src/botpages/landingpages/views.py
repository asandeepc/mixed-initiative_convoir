from django.template import RequestContext
from django.shortcuts import render
from django.http import HttpResponse
from slackclient import SlackClient
import requests
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect
from .models import wizardUsers, collabUsers, wizardInterface, interactionEvents
import calendar
from bs4 import BeautifulSoup
import time
import json
import re
import urllib.request as ur
from django.http import JsonResponse
# Create your views here.

BOT_ID= "" ## Setup the bot id here. 
EXP_CHANNEL= "" ## Set the Slack channel id here. 
SLACK_BOT_TOKEN=''## Set the token id. You get this when you first create the app. 
AT_BOT = "<@" + BOT_ID + ">" # don't use the :; HATE it!
EXAMPLE_COMMAND = "do"
slack_client = SlackClient(SLACK_BOT_TOKEN)
#BASEURL = "https://sils-jasrv.ad.unc.edu:8443/info_request/nyt/search.jsp" ## The results are being retrieved from Jaime's server.
bar_color = "#D3D3D3"

search_url = "https://api.cognitive.microsoft.com/bing/v5.0/search"
subscription_key = '' ## get this key from the bing web api. 
headers = {"Ocp-Apim-Subscription-Key" : subscription_key}

backend_logging_serverURL = ''

landing_page = 'http://127.0.0.1:8000/landing/?q=' ## Change this when on production.
# landing_page = 'https://botpages.herokuapp.com/landing/?q=' ## In production. 

def user_login(request):
	print("hello world!")
	entry_questionnaire = "" # Custom
	pre_task_questionnaire = "" # Custom
	post_task_questionnaire = "" # Custom

	if request.GET.get('user_id'):
		id = re.sub(r'\s+', '', request.GET.get('user_id').lower())
		print("here is the id:"+str(id))
		split_id = id.split("_")
		user_id = split_id[0]
		task_order = list(split_id[1])
		condition_order = list(split_id[2])
		print(user_id)
		print(task_order)
		print(condition_order)

		task_1 = task_order[0]
		task_2 = task_order[1]
		task_3 = task_order[2]

		condition_1 = condition_order[0]
		condition_2 = condition_order[1]
		condition_3 = condition_order[2]
		context = {
				"entry_questionnaire":entry_questionnaire+str("/?pid=")+str(user_id),
				"pre_task_questionnaire":pre_task_questionnaire,
				"post_task_questionnaire":post_task_questionnaire,
				"pre_task1":pre_task_questionnaire+str("/?task=")+str(task_1)+str("&condition=")+str(condition_1)+str("&pid=")+str(user_id),
				"pre_task2":pre_task_questionnaire+str("/?task=")+str(task_2)+str("&condition=")+str(condition_2)+str("&pid=")+str(user_id),
				"pre_task3":pre_task_questionnaire+str("/?task=")+str(task_3)+str("&condition=")+str(condition_3)+str("&pid=")+str(user_id),
				"post_task1":post_task_questionnaire+str("/?task=")+str(task_1)+str("&condition=")+str(condition_1)+str("&pid=")+str(user_id),
				"post_task2":post_task_questionnaire+str("/?task=")+str(task_2)+str("&condition=")+str(condition_2)+str("&pid=")+str(user_id),
				"post_task3":post_task_questionnaire+str("/?task=")+str(task_3)+str("&condition=")+str(condition_3)+str("&pid=")+str(user_id),
				"questionnaire_pages":True,
		}
		return render(request,'user_login.html',context)

	return render(request,'user_login.html',{})


def slash_response(request):
	context_instance=RequestContext(request)
	print(context_instance)
	print("Printing the response:"+str(request))
	data = {
        'response_type': 'in_channel',
        'text': 'got it',
        'request':str(request)
    }
	return JsonResponse(data)


def logger_userPages(request,*args,**kwargs):
	print("hello world!")


'''

In the previous pilots, the landing page took the focus away from the conversations. So, in the next pilot we won't be using the landing page.

'''
def landing_view(request, *args,**kwargs):
	query = request.GET.get('q')
	if query:
		params  = {"q": query, "textDecorations":True, "textFormat":"HTML","count":50}
		response = requests.get(search_url, headers=headers, params=params)
		response.raise_for_status()
		search_results = response.json()['webPages']['value']

		queryset_list = search_results
		paginator = Paginator(queryset_list, 10)
		page = request.GET.get('page',1)
		queryset_list = paginator.page(page)
		context = {
			"results": queryset_list,
			"title": "Web Results",
		}
		return render(request,'landing.html',context)
	return render(request,'landing.html',{})

def wizard_login(request, *args,**kwargs):
	print("Here we are at the wizard login")
	print(request)

	if request.GET.get('wizard_id') and request.GET.get('session_id'):
		wizard_id = request.GET.get('wizard_id').lower()
		session_id = request.GET.get('session_id').lower()
		if wizard_id in set(['m1', 'm2', 'm3','admin']):
			print("correct wizard id")
			request.session['wizard_id'] = wizard_id
		else:
			print("wrong wizard id")
		print("This is the session_id:"+str(session_id))
		wiz_session = wizardUsers(wizard_id=wizard_id,session_id=session_id) ## making the wizard data object.
		wiz_session.save()
		print("wizard's unique session id is:"+str(wiz_session.id))
		request.session['session_id'] = session_id
		request.session['wiz_pk_id'] = wiz_session.id # This is the primary key of the wizard's login.
		return redirect('wizard_view')
	return render(request,'wizard_login.html',{})

def logout(request,*args,**kwargs):
	request.session.flush()
	return render(request,'logout.html',{})

def test(request, *args,**kwargs):
	wizard_id = request.session['wizard_id']
	session_id = request.session['session_id']
	return render(request,'test.html',{})

def interaction_data(request, *args,**kwargs): ## All the interaction data will be stored in the database from this function.
	print("So we are here now")
	interface_key = request.session['wiz_interface_session_id']
	interaction_type = request.GET.get('type')
	wizard_interface_key_obj = wizardInterface.objects.get(pk=interface_key)

	## Here under which ever event type we fall in, store the appropriate variables.

	if interaction_type == 'click':
		print("Interaction type:"+str(interaction_type))
		print(request)
		interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
											value = request.GET.get('quant'),url=request.GET.get('url'),
											session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
											searchbot_condition = request.session['searchbot_condition']
											)
		interaction_event.save()


	if interaction_type == 'send_result':
		print("Interaction type:"+str(interaction_type))
		print(request)
		interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
											value = request.GET.get('quant'),url=request.GET.get('url'),
											session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
											searchbot_condition = request.session['searchbot_condition']
											)
		interaction_event.save()
		request.session['interaction_event_id'] = interaction_event.id
		wizard_view(request)


	if interaction_type == 'chat_message':
		## Come back and check for the case, where the users may send a message without first issuing a query.
		print("Interaction type:"+str(interaction_type))
		print(request)
		interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
											value = request.GET.get('quant'),url="None",
											session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
											searchbot_condition = request.GET.get('condition')
											)
		interaction_event.save()
		wizard_view(request)

	if interaction_type == 'mouseenter':
		print("Interaction type:"+str(interaction_type))
		print(request)
		interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
											value = request.GET.get('quant'),url=request.GET.get('url'),
											session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
											searchbot_condition = request.session['searchbot_condition']
											)
		interaction_event.save()

	if interaction_type == 'scroll':
		print("Interaction type:"+str(interaction_type))
		print(request)
		interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
											value = request.GET.get('quant'),url="None",
											session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
											searchbot_condition = request.session['searchbot_condition']
											)
		interaction_event.save()

	return HttpResponse("OK")


def wizard_view(request, *args,**kwargs):
	query = request.GET.get('q')
	if query:
		query = re.sub('[^a-zA-Z0-9 \n\.]', '', query)
		print(query)
	questions = request.GET.get('ques')
	suggestions = request.GET.get('sugg')

	if "sendresult" in request.GET:
		print("sending the sendresult here")
		print(request.session['interaction_event_id'])
		server_url = backend_logging_serverURL+'?url='+str(request.GET.get('url')) + '&rank=' + str(request.GET.get('quant')) + '&interface_id='+str(request.session['interaction_event_id'])+'&condition='+ str(request.session['searchbot_condition'])
		attachment = [{"title": BeautifulSoup(request.GET.get('title'), 'html.parser').get_text().replace("?","'"),
                         "title_link": server_url,#request.GET.get('url'),#returnHTTPSurl(BeautifulSoup(result['url'], 'html.parser').get_text()),
                         "color": bar_color,
                         "text":BeautifulSoup(request.GET.get('snippet'), 'html.parser').get_text().replace("?", "'"),
                         "ts": getEpoch(request.GET.get('date')),
						 "author_name": str(request.GET.get('displayURL')),#str(request.GET.get('displayURL'))
                         }]
		slack_client.api_call("chat.postMessage", channel=EXP_CHANNEL, as_user=True,
                              unfurl_links=True,attachments=attachment) ## Get the testing channel logs here.

	if "searchbtn" in request.GET:
		print("came to search")
		params  = {"q": query, "textDecorations":True, "textFormat":"HTML","count":50}
		response = requests.get(search_url, headers=headers, params=params)
		response.raise_for_status()
		search_results = response.json()['webPages']['value']
		queryset_list = search_results
		paginator = Paginator(queryset_list, 10)
		page = request.GET.get('page',1)
		queryset_list = paginator.page(page)
		context = {
			"results": queryset_list,
			"title": "Web Results",
		}
		print("type is:"+str(type(request.session['wiz_pk_id'])))
		if request.GET.get('condition'):
			request.session['searchbot_condition'] = int(request.GET.get('condition'))

		if "page" in request.GET:
			print("READY for some pagination ----- >>>>>") ## For pagination, the queries get back here, and so we don't want to double record those.
			print(request.GET)
			interface_key = request.session['wiz_interface_session_id']
			interaction_type = "pagination"
			page = request.GET.get('page')
			wizard_interface_key_obj = wizardInterface.objects.get(pk=interface_key)
			interaction_event = interactionEvents(event=interaction_type, wizard_id=request.session['wizard_id'],
												value = page,url="None",
												session_id=request.session['session_id'],interface_id=wizard_interface_key_obj,
												searchbot_condition = request.session['searchbot_condition']
												)
			interaction_event.save()
			return render(request,'wizard.html',context)
		else:
			wizard_table_key_obj = wizardUsers.objects.get(pk=request.session['wiz_pk_id'])
			wiz_interface = wizardInterface(query = query, searchbot_condition = request.session['searchbot_condition'], results = search_results, wizard_id = request.session['wizard_id'],
											session_id = request.session['session_id'],wizard_table_key = wizard_table_key_obj)
			wiz_interface.save()
			request.session['query'] = query
			request.session['wiz_interface_session_id'] = wiz_interface.id
			print("Here are the simple search results request:"+str(request))
			return render(request,'search_results.html',context)
### Sending the questions and suggestions to slack.
	if questions:
		print("Ready to push the following content:"+str(questions))
		slack_client.api_call("chat.postMessage", channel=EXP_CHANNEL, as_user=True, text=questions,
                              unfurl_links=True)
	if suggestions:
		slack_client.api_call("chat.postMessage", channel=EXP_CHANNEL, as_user=True, text=suggestions, unfurl_links=True)
	return render(request,'wizard.html',{})

######### All the code below, is to insert content right inside Slack. #########

def removeQuotes(query):
    query = re.sub('\s+', ' ',re.sub(r'[^\w]', ' ', query).strip()).strip()
    return query


def returnHTTPSurl(url):
    if url.startswith("http"):
        return url
    else:
        return "https://"+url

def getEpoch(date):
    pattern = '%Y-%m-%dT%H:%M:%S'  # '%d.%m.%Y %H:%M:%S'
    #pattern = '%A %B %d, %Y'
    utc_epoch = calendar.timegm(time.strptime(date, pattern))
    print("Time:" + str(utc_epoch))
    return utc_epoch



## ------------------------- This is the place for the practice code that we will be using to send the wizards. ## -------------------------

def wiztest(request):
	TEST_CHANNEL = '' # Custom 

	query = request.GET.get('q')
	if query:
		query = re.sub('[^a-zA-Z0-9 \n\.]', '', query)
		print(query)
	questions = request.GET.get('ques')
	suggestions = request.GET.get('sugg')

	if "sendresult" in request.GET:
		print("sending result here")
		attachment = [{"title": BeautifulSoup(request.GET.get('title'), 'html.parser').get_text().replace("?","'"),
                         "title_link": request.GET.get('url'),#returnHTTPSurl(BeautifulSoup(result['url'], 'html.parser').get_text()),
                         "color": bar_color,
                         "text":BeautifulSoup(request.GET.get('snippet'), 'html.parser').get_text().replace("?", "'"),
                         "ts": getEpoch(request.GET.get('date')),
						 "author_name": str(request.GET.get('displayURL')),#str(request.GET.get('displayURL'))
                         }]
		slack_client.api_call("chat.postMessage", channel=TEST_CHANNEL, as_user=True,
                              unfurl_links=True,attachments=attachment)
		# slack_client.api_call("chat.postMessage", channel=TEST_CHANNEL, as_user=True, title='Hello world!',title_link='https://api.slack.com/docs/message-link-unfurling',text=request.GET.get('url'),
        #                       unfurl_links=True) ## Get the testing channel logs here.

	if "searchbtn" in request.GET:
		print("came to search")
		params  = {"q": query, "textDecorations":True, "textFormat":"HTML","count":50}
		response = requests.get(search_url, headers=headers, params=params)
		response.raise_for_status()
		search_results = response.json()['webPages']['value']
		queryset_list = search_results
		paginator = Paginator(queryset_list, 10)
		page = request.GET.get('page',1)
		queryset_list = paginator.page(page)
		context = {
			"results": queryset_list,
			"title": "Web Results",
		}
		if "page" in request.GET:
			page = request.GET.get('page')
			return render(request,'wizard_test.html',context)
		else:
			return render(request,'search_results_test.html',context)
### Sending the questions and suggestions to slack.
	if questions:
		slack_client.api_call("chat.postMessage", channel=TEST_CHANNEL, as_user=True, text=questions,
                              unfurl_links=True)
	if suggestions:
		slack_client.api_call("chat.postMessage", channel=TEST_CHANNEL, as_user=True, text=suggestions, unfurl_links=True)
	return render(request,'wizard_test.html',{})
