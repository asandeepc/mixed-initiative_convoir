// Getting all the scrolls here.
$(document).ready(function() {

    $('.btn.btn-success').mousedown(function() { // This is to log the result that the Wizard is sending.
        // This is sent after the search results are populated. Using that we can set the bot condition value in the session.
        console.log('Ready to push the result man');
        var element_forrank = ($(this).parent()).parent();
        var url = element_forrank.attr('data-url');
        var raw_id = element_forrank.attr('data-id');
        console.log(raw_id);
        console.log(element_forrank.attr('data-displayUrl'));
        console.log(element_forrank.attr('data-snippet'));
        console.log(element_forrank.attr('data-date'));
        var rank_i = parseInt(raw_id.substring(raw_id.lastIndexOf('.')+1,raw_id.length)) + 1;
        $.get('/wiztest/',{url:url,quant:rank_i,sendresult:true, type:"send_result", interaction:true,
                                    displayURL:element_forrank.attr('data-displayUrl'),snippet:element_forrank.attr('data-snippet'),
                                    date:element_forrank.attr('data-date'),title:element_forrank.attr('data-title')});
      });

});
