$(document).ready(function() {
    $("#search1").submit(function(e){ //
      e.preventDefault();
      var query = $("input[name='q']").val();
      console.log("search query1");
      $.get("/wizard/", { q:query, searchbtn:"",condition:"1"},function(data) {
        $("#getresults").html(data);
        console.log('Here we are again baby!');
        window.history.pushState(null, "", "/wizard"); // The pagination is leaving a bunch of params in the url. I'm doing this to clear out all those params.
      });
    });

    $("#search2").submit(function(e){ // Condition 3 suggestion.
      e.preventDefault();
      var query = $("input[name='q2']").val();
      // Now gonna make the ajax call to the server.
      console.log("search query2");
      console.log(query);
      $.get("/wizard/", { q:query, searchbtn:"",condition:"2"},function(data) {
        $("#getresults").html(data);
        window.history.pushState(null, "", "/wizard");
      });
    });

    $("#search3").submit(function(e){ // Condition 3 suggestion.
      e.preventDefault();
      var query = $("input[name='q3']").val();
      console.log("search query2");
      // Now gonna make the ajax call to the server.
      $.get("/wizard/", { q:query, searchbtn:"",condition:"3"},function(data) {
        $("#getresults").html(data);
        window.history.pushState(null, "", "/wizard");
      });
    });

});
