// Getting all the scrolls here.
$(document).ready(function() {
    console.log("Inside interactions");
    $(window).scroll(function(){
      var scroll_pos = $(window).scrollTop();
      var doc_height = $(document).height();
      var cont_height = $(window).height();
      var scroll_pct = (scroll_pos / (doc_height - cont_height)) * 100;
      scroll_pct = Math.round(scroll_pct * 100) / 100;
      //console.log(scroll_pct)
      $.get("/interaction_data/", { type:"scroll",quant:scroll_pct, interaction:true });
    });
    /// Here we are logging all the mouseenters.

    $('.my-3.p-3.bg-white.rounded.box-shadow.bg-purple').mouseenter(function() {
      var rank = parseInt($(this).data('id').substring($(this).data('id').lastIndexOf('.')+1,$(this).data('id').length)) + 1;
      //console.log("Mouse enter:"+rank);
      //console.log($(this).data('url'));
      //console.log("Ok, done here"); // bd-callout bd-callout-info bd-callout-shadow my-3 p-3 bg-white rounded box-shadow
      $.get("/interaction_data/", { type:"mouseenter",quant:rank, interaction:true,url:$(this).data('url') });
    });
    // Logging all the clicks.
    $('.my-3.p-3.bg-white.rounded.box-shadow.bg-purple a').mousedown(function() {
      var rank = parseInt($(this).data('id').substring($(this).data('id').lastIndexOf('.')+1,$(this).data('id').length)) + 1;
      //console.log("Rank of the link clicked:"+rank);
      //console.log($(this).data('url'));
      //console.log("Ok, done here");
      $.get("/interaction_data/", { type:"click",quant:rank, interaction:true,url:$(this).data('url') });
    });

    $('.btn.btn-success').mousedown(function() { // This is to log the result that the Wizard is sending.
        // This is sent after the search results are populated. Using that we can set the bot condition value in the session.
        console.log('Ready to push the result man');
        var element_forrank = ($(this).parent()).parent();
        var url = element_forrank.attr('data-url');
        var raw_id = element_forrank.attr('data-id');
        console.log(raw_id);
        console.log(element_forrank.attr('data-displayUrl'));
        console.log(element_forrank.attr('data-snippet'));
        console.log(element_forrank.attr('data-date'));
        var rank_i = parseInt(raw_id.substring(raw_id.lastIndexOf('.')+1,raw_id.length)) + 1;
        $.get('/interaction_data/',{url:url,quant:rank_i,sendresult:true, type:"send_result", interaction:true,
                                    displayURL:element_forrank.attr('data-displayUrl'),snippet:element_forrank.attr('data-snippet'),
                                    date:element_forrank.attr('data-date'),title:element_forrank.attr('data-title')});
      });

});
